# Place multiple GameObject using agumented reality in real world

## Output of the code:

![alt text](https://gitlab.com/rahulmool/placemultiplegameobjectarfoundation/-/raw/master/dataforreadme/image1.jpeg)

## Link to youtube video:

[![IMAGE ALT TEXT HERE](https://img.youtube.com/vi/UmZSeZidLcw/0.jpg)](https://youtu.be/UmZSeZidLcw)

[ARKit 3 with Unity3d and AR Foundation - Adding Plane Detection and A Placement Controller](https://youtu.be/UmZSeZidLcw)

# Problem:
- cant change gameobject to textmeshpro
